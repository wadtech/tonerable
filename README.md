# Tonerable

*Easy Printer Consumable Inventory and Ordering.*

[![Build Status](https://travis-ci.org/wadtech/tonerable.png?branch=master)](https://travis-ci.org/wadtech/tonerable)
[![Dependency Status](https://gemnasium.com/wadtech/tonerable.png)](https://gemnasium.com/wadtech/tonerable)
[![Code Climate](https://codeclimate.com/github/wadtech/tonerable.png)](https://codeclimate.com/github/wadtech/tonerable)

## About

Tonerable is an easy-to-use application for stock control, ordering and reporting for printer consumables.

## Technical

Tonerable uses AngularJS for the client side, making it a modern and flexible application. Backing it up is Ruby on Rails offering a JSON REST API which can easily be interfaced with other software.

## License

Copyright (c) 2013 Peter Mellett

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
