basePath = '.';
files = [
  ANGULAR_SCENARIO,
  ANGULAR_SCENARIO_ADAPTER,
  '*_spec.js'
];
exclude        = [];
reporters      = ['progress'];
port           = 8080;
runnerPort     = 9100;
colors         = true;
logLevel       = LOG_INFO;
autoWatch      = true;
browsers       = ['Chrome'];
captureTimeout = 10000;
singleRun      = false;
proxies        = { '/': 'http://localhost:3000/' };