describe('Home Page', function() {
  beforeEach(function() {
    browser().navigateTo('/');
  });
  it('should have the header', function() {
    expect(element('body').html()).toContain('Tonerable')
  });
  it('should have the navbar', function() {
    expect(element('body').html()).toContain('navbar')
  });
});