require 'spec_helper'

describe Supplier do
  it "should have a valid factory" do
    supplier = FactoryGirl.build(:supplier)
    supplier.should be_valid
  end

  it "should have a valid name" do
    supplier = FactoryGirl.build(:supplier)
    supplier.name.should_not be_nil
    supplier.name.should_not eq ''
  end

  it "does not have to have a url" do
    supplier = FactoryGirl.build(:supplier, :url => nil)
    supplier.should be_valid
  end

  it "must have a unique name" do
    supplier = FactoryGirl.create(:supplier)
    new_supplier = FactoryGirl.build(:supplier)

    new_supplier.should_not be_valid
  end
end
