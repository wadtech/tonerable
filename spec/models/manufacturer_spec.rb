require 'spec_helper'

describe Manufacturer do
  it "should have a valid factory" do
    manufacturer = FactoryGirl.build(:manufacturer)
    manufacturer.should be_valid
  end

  it "should have a valid name" do
    manufacturer = FactoryGirl.build(:manufacturer)
    manufacturer.name.should_not be_nil
    manufacturer.name.should_not eq ''
  end

  it "does not have to have a url" do
    manufacturer = FactoryGirl.build(:manufacturer, :url => nil)
    manufacturer.should be_valid
  end

  it "must have a unique name" do
    manufacturer = FactoryGirl.create(:manufacturer)
    new_manufacturer = FactoryGirl.build(:manufacturer)

    new_manufacturer.should_not be_valid
  end
end
