require 'spec_helper'

describe ApplicationHelper do
  describe 'viewport_tag' do
    it "generates the meta viewport tag" do
      attempts = {
        '1'      => 1.0,
        'string' => 0.0,
        '0.5'    => 0.5,
        '3.1'    => 3.1
      }

      attempts.each do |param, expected|
        _expected = "<meta content=\"width=device-width, initial-scale=#{expected}\" name=\"viewport\"></meta>"
        helper.viewport_tag(param).should eq _expected
      end
    end
  end
end
