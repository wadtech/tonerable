require 'spec_helper'

describe SuppliersController do
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all suppliers as @suppliers" do
      supplier = FactoryGirl.create(:supplier)
      get :index, {}, valid_session
      assigns(:suppliers).should eq([supplier])
    end
  end

  describe "GET show" do
    it "assigns the requested supplier as @supplier" do
      supplier = FactoryGirl.create(:supplier)
      get :show, {:id => supplier.to_param}, valid_session
      assigns(:supplier).should eq(supplier)
    end
  end

  describe "GET new" do
    it "assigns a new supplier as @supplier" do
      get :new, {}, valid_session
      assigns(:supplier).should be_a_new(Supplier)
    end
  end

  describe "GET edit" do
    it "assigns the requested supplier as @supplier" do
      supplier = FactoryGirl.create(:supplier)
      get :edit, {:id => supplier.to_param}, valid_session
      assigns(:supplier).should eq(supplier)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Supplier" do
        expect {
          post :create, {:supplier => FactoryGirl.attributes_for(:supplier)}, valid_session
        }.to change(Supplier, :count).by(1)
      end

      it "assigns a newly created supplier as @supplier" do
        post :create, {:supplier => FactoryGirl.attributes_for(:supplier)}, valid_session
        assigns(:supplier).should be_a(Supplier)
        assigns(:supplier).should be_persisted
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved supplier as @supplier" do
        # Trigger the behavior that occurs when invalid params are submitted
        Supplier.any_instance.stub(:save).and_return(false)
        post :create, {:supplier => FactoryGirl.attributes_for(:supplier, :name => "invalid value")}, valid_session
        assigns(:supplier).should be_a_new(Supplier)
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested supplier" do
        supplier = FactoryGirl.create(:supplier)
        # Assuming there are no other suppliers in the database, this
        # specifies that the Supplier created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        Supplier.any_instance.should_receive(:update_attributes).with({ "name" => "MyString" })
        put :update, {:id => supplier.to_param, :supplier => { "name" => "MyString" }}, valid_session
      end

      it "assigns the requested supplier as @supplier" do
        supplier = FactoryGirl.create(:supplier)
        put :update, {:id => supplier.to_param, :supplier => FactoryGirl.attributes_for(:supplier)}, valid_session
        assigns(:supplier).should eq(supplier)
      end
    end

    describe "with invalid params" do
      it "assigns the supplier as @supplier" do
        supplier = FactoryGirl.create(:supplier)
        # Trigger the behavior that occurs when invalid params are submitted
        Supplier.any_instance.stub(:save).and_return(false)
        put :update, {:id => supplier.to_param, :supplier => { "name" => "invalid value" }}, valid_session
        assigns(:supplier).should eq(supplier)
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested supplier" do
      supplier = FactoryGirl.create(:supplier)
      expect {
        delete :destroy, {:id => supplier.to_param}, valid_session
      }.to change(Supplier, :count).by(-1)
    end
  end
end
