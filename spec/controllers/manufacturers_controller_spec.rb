require 'spec_helper'

describe ManufacturersController do
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all manufacturers as @manufacturer" do
      manufacturer = FactoryGirl.create(:manufacturer)
      get :index, {}, valid_session
      assigns(:manufacturers).should eq([manufacturer])
    end
  end

  describe "GET show" do
    it "assigns the requested manufacturer as @manufacturer" do
      manufacturer = FactoryGirl.create(:manufacturer)
      get :show, {:id => manufacturer.to_param}, valid_session
      assigns(:manufacturer).should eq(manufacturer)
    end
  end

  describe "GET new" do
    it "assigns a new manufacturer as @manufacturer" do
      get :new, {}, valid_session
      assigns(:manufacturer).should be_a_new(Manufacturer)
    end
  end

  describe "GET edit" do
    it "assigns the requested manufacturer as @manufacturer" do
      manufacturer = FactoryGirl.create(:manufacturer)
      get :edit, {:id => manufacturer.to_param}, valid_session
      assigns(:manufacturer).should eq(manufacturer)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Manufacturer" do
        expect {
          post :create, {:manufacturer => FactoryGirl.attributes_for(:manufacturer)}, valid_session
        }.to change(Manufacturer, :count).by(1)
      end

      it "assigns a newly created manufacturer as @manufacturer" do
        post :create, {:manufacturer => FactoryGirl.attributes_for(:manufacturer)}, valid_session
        assigns(:manufacturer).should be_a(Manufacturer)
        assigns(:manufacturer).should be_persisted
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved manufacturer as @manufacturer" do
        # Trigger the behavior that occurs when invalid params are submitted
        Manufacturer.any_instance.stub(:save).and_return(false)
        post :create, {:manufacturer => FactoryGirl.attributes_for(:manufacturer, :name => "invalid value")}, valid_session
        assigns(:manufacturer).should be_a_new(Manufacturer)
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested manufacturer" do
        manufacturer = FactoryGirl.create(:manufacturer)
        # Assuming there are no other manufacturer in the database, this
        # specifies that the Manufacturer created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        Manufacturer.any_instance.should_receive(:update_attributes).with({ "name" => "MyString" })
        put :update, {:id => manufacturer.to_param, :manufacturer => { "name" => "MyString" }}, valid_session
      end

      it "assigns the requested manufacturer as @manufacturer" do
        manufacturer = FactoryGirl.create(:manufacturer)
        put :update, {:id => manufacturer.to_param, :manufacturer => FactoryGirl.attributes_for(:manufacturer)}, valid_session
        assigns(:manufacturer).should eq(manufacturer)
      end
    end

    describe "with invalid params" do
      it "assigns the manufacturer as @manufacturer" do
        manufacturer = FactoryGirl.create(:manufacturer)
        # Trigger the behavior that occurs when invalid params are submitted
        Manufacturer.any_instance.stub(:save).and_return(false)
        put :update, {:id => manufacturer.to_param, :manufacturer => { "name" => "invalid value" }}, valid_session
        assigns(:manufacturer).should eq(manufacturer)
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested manufacturer" do
      manufacturer = FactoryGirl.create(:manufacturer)
      expect {
        delete :destroy, {:id => manufacturer.to_param}, valid_session
      }.to change(Manufacturer, :count).by(-1)
    end
  end
end
