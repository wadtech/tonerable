Tonerable::Application.routes.draw do
  resources :manufacturers, :defaults => { :format => 'json' }
  resources :suppliers,     :defaults => { :format => 'json' }

  root :to => "front#index"
end
