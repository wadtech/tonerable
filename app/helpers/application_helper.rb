module ApplicationHelper
  #default initial scale, should be a float
  def viewport_tag(default)
    content_tag :meta, '', :content => "width=device-width, initial-scale=#{default.to_f}", :name => "viewport"
  end
end
