# JSON Endpoints for Supplier model.
class SuppliersController < ApplicationController
  respond_to :json

  # GET /suppliers.json
  def index
    @suppliers = Supplier.all

    respond_with @suppliers
  end

  # GET /suppliers/1.json
  def show
    @supplier = Supplier.find(params[:id])

    respond_with @supplier
  end

  # GET /suppliers/new.json
  def new
    @supplier = Supplier.new

    respond_with @supplier
  end

  # GET /suppliers/1/edit
  def edit
    @supplier = Supplier.find(params[:id])

    respond_with @supplier
  end

  # POST /suppliers.json
  def create
    @supplier = Supplier.new(params[:supplier])

    if @supplier.save
      respond_with @supplier, status: :created, location: @supplier
    else
      respond_with @supplier, status: :unprocessable_entity
    end
  end

  # PUT /suppliers/1.json
  def update
    @supplier = Supplier.find(params[:id])

    if @supplier.update_attributes(params[:supplier])
      respond_with success: true
    else
      respond_with @supplier, status: :unprocessable_entity
    end
  end

  # DELETE /suppliers/1.json
  def destroy
    @supplier = Supplier.find(params[:id])
    @supplier.destroy

    respond_with success: true
  end
end
