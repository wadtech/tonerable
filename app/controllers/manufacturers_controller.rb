# JSON Endpoints for Manufacturer model.
class ManufacturersController < ApplicationController
  respond_to :json

  # GET /manufacturers.json
  def index
    @manufacturers = Manufacturer.all

    respond_with @manufacturers
  end

  # GET /manufacturers/1.json
  def show
    @manufacturer = Manufacturer.find(params[:id])

    respond_with @manufacturer
  end

  # GET /manufacturers/new.json
  def new
    @manufacturer = Manufacturer.new

    respond_with @manufacturer
  end

  # GET /manufacturers/1/edit
  def edit
    @manufacturer = Manufacturer.find(params[:id])

    respond_with @manufacturer
  end

  # POST /manufacturers.json
  def create
    @manufacturer = Manufacturer.new(params[:manufacturer])

    if @manufacturer.save
      respond_with @manufacturer, status: :created, location: @manufacturer
    else
      respond_with @manufacturer, status: :unprocessable_entity
    end
  end

  # PUT /manufacturers/1.json
  def update
    @manufacturer = Manufacturer.find(params[:id])

    if @manufacturer.update_attributes(params[:manufacturer])
      respond_with success: true
    else
      respond_with @manufacturer, status: :unprocessable_entity
    end
  end

  # DELETE /manufacturers/1.json
  def destroy
    @manufacturer = Manufacturer.find(params[:id])
    @manufacturer.destroy

    respond_with success: true
  end
end
