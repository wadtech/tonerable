app = angular.module("Tonerable")

app.factory "Supplier", ["$resource", ($resource) ->
    $resource("/suppliers/:id", {id: "@id"}, {update: { method: "PUT" }})
]

@SupplierCtrl = ["$scope", "Supplier", ($scope, Supplier) ->
  $scope.suppliers = Supplier.query()

  $scope.addSupplier = ->
    suppliers = $scope.suppliers

    if suppliers.length > 0
      $scope.newSupplier.setValidity('uniqueName', false)

    if $scope.newSupplier.$valid
      supplier = Supplier.save($scope.newSupplier)
      $scope.suppliers.push(supplier)
      $scope.newSupplier = {}
]
