app = angular.module("Tonerable")

app.factory "Manufacturer", ["$resource", ($resource) ->
    $resource("/manufacturers/:id", {id: "@id"}, {update: { method: "PUT" }})
]

@ManufacturerCtrl = ["$scope", "Manufacturer", ($scope, Manufacturer) ->
  $scope.manufacturers = Manufacturer.query()

  $scope.addManufacturer = ->
    manufacturers = $scope.manufacturers

    if manufacturers.length > 0
      $scope.newManufacturer.setValidity('uniqueName', false)

    if $scope.newManufacturer.$valid
      manufacturer = Manufacturer.save($scope.newManufacturer)
      $scope.manufacturers.push(manufacturer)
      $scope.newManufacturer = {}
]