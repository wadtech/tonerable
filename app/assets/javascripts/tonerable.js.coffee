app = angular.module("Tonerable", ['ngResource'])

app.config(['$routeProvider', ($routeProvider) ->
  $routeProvider.when('/suppliers', { templateUrl: '/angularjs/partials/suppliers.html' })
  $routeProvider.when('/manufacturers', { templateUrl: '/angularjs/partials/manufacturers.html' })
  $routeProvider.otherwise({redirectTo: '/', templateUrl: '/angularjs/dashboard/index.html' })
])

app.config(['$locationProvider', ($locationProvider) ->
  $locationProvider.html5Mode(true);
])