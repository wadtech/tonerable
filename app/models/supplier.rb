class Supplier < ActiveRecord::Base
  attr_accessible :name, :url

  validates :name, presence: true
  validates :name, uniqueness: true
end
